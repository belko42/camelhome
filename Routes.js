const express = require('express');
const router = express.Router();

module.exports = ( Rest ) => {

    // Rest.app << express

    // https://www.camelhome.ru/
    router.all("/", (req, res) => {
        console.log('https://www.camelhome.ru/ - OK');
        res.send("OK");
    });

    Rest.app.use( router );

}