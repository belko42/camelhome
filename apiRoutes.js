const express = require('express');
const router = express.Router();

/* ROUTES */
const api_all = require('./routes/api.all');
const api_get_auth = require('./routes/api.get.auth');
const api_post_token = require('./routes/api.post.token');
const api_get_v1_0 = require('./routes/api.get.v1.0');
const api_get_user_devices = require('./routes/api.get.user.devices');
const api_post_user_devices_query = require('./routes/api.post.user.devices.query');
const api_post_user_devices_action = require('./routes/api.post.user.devices.action');
const api_post_user_unlink = require('./routes/api.post.user.unlink');
/* /ROUTES */

module.exports = async ( Rest ) => {

// Rest.app << == express
// Rest.app.api << == express [subdomain]

    // https://api.camelhome.ru/
    router.use( await api_all( Rest ) );

    // https://api.camelhome.ru/auth
    router.use( await api_get_auth( Rest ) );

    // https://api.camelhome.ru/token
    router.use( await api_post_token( Rest ) );

    // https://api.camelhome.ru/v1.0/
    router.use( await api_get_v1_0( Rest ) );

    // https://api.camelhome.ru/v1.0/user/devices
    router.use( await api_get_user_devices( Rest ) );

    // https://api.camelhome.ru/v1.0/user/devices/query
    router.use( await api_post_user_devices_query( Rest ) );

    // https://api.camelhome.ru/v1.0/user/devices/action
    router.use( await api_post_user_devices_action( Rest ) );

    // https://api.camelhome.ru/v1.0/user/unlink
    router.use( await api_post_user_unlink( Rest ) );

    // https://api.camelhome.ru/ping
    router.get("/ping", async(req, res) => {
        try {
            console.ok('https://api.camelhome.ru/ping');
            console.line();

            console.info("Response - OK");
            res.json({success: true, message: 'OK', data: {}});
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    Rest.app.api.use( router );

}