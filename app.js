const logger = require('mii-logger.js');
const path = require('path');
const fs = require('fs');
const dotenv = require('dotenv').config();
const config = require('./config');

const restApi = require('./restApi');
new restApi( config );