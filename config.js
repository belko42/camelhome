module.exports = {
    express: {
        port: 3000,
    },

    alice: {
        id_oauth: '530d2049a98f44589c02e270c8bac7a7',
        secret_oauth: 'cux7l8yHagdL36IYzSZf',
        code_oauth: 'vS82WnlKx9',
        id_dialog: 'ecfd1b45-198a-4246-b850-27e8bbd8ddee',
    },

    mqtt: {
        host: 'ws://mqtt.camelhome.ru',
        option : {
            port: 80,
            username: 'camelhome',
            password: 'camelhome',
            clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8), //Не изменять
        },
    },

    mongodb: {
        username: 'camelhome',
        password: 'camelhome',
        host: '217.25.89.129',
        port: '27017',
        dbName: 'camelhome',
    },

    // type - https://tech.yandex.ru/dialogs/alice/doc/smart-home/concepts/main-objects-docpage/

    devices: [
        {
            name: 'Свет',
            room: 'Комната',
            type: 'devices.types.light',
            mqtt: {
                set: 'ROOT/BEDROOM/Light',
                status: 'ROOT/BEDROOM/Light/status'
            }
        }
    ]
}