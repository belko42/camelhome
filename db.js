const { MongoClient } = require('mongodb');


module.exports = async ( App ) => {
    try{

        const username = process.env.DB_USERNAME;
        const password = process.env.DB_PASSWORD;
        const host     = process.env.DB_HOST;
        const port     = process.env.DB_PORT;
        const dbName   = process.env.DB_NAME;


        const urlMongo = `mongodb://${username}:${password}@${host}:${port}/${dbName}`;
        const mongoDB = new MongoClient(urlMongo, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

        const DB = await mongoDB.connect();
        const DBs = DB.db(dbName);
        console.ok(' MongoDB - init ');
        return DBs;
    }catch(e){
        console.error(` MongoClient: ${e.message} `);
        return false;
    }
}