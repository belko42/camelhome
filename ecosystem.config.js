module.exports = {
    apps : [
        {
            name: 'CamelHomeAPI',
            script: './app.js',
        }
    ],

    deploy : {
        production : {
            user : 'ftp-user',
            host : '217.25.89.129',
            ref  : 'origin/develop',
            repo : 'git@bitbucket.org:belko42/camelhome.git',
            path : '/var/www/camelhome.ru',
            'pre-deploy-local': '',
            'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production --max-memory-restart 100M',
            'pre-setup': ''
        }
    }
};
