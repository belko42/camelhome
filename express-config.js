const bodyParser = require("body-parser");
const express = require("express");

module.exports = async ( App ) => {
    const app = express();

     // прикручиваем к нему модуль для обработки post запросов
    app.use( bodyParser.json() );

    // сразу же после того как прикрутили модуль, отлавливаем ошибки
    // если коиент передал битый json, то модуль который идет выше, выкинит Exeption,
    // мы его тут же отлавливаем, и говорим клиенту, что он передал что то битое.
    app.use (function (error, req, res, next){
        if( error ){
            console.error(` #main: [0]: [broken-json] ${error.message}`);
            return res.json({success: false, message: 'broken-json' });
        }
        next();
    });

    // прикручиваем к нему другой модуль
    app.use(bodyParser.urlencoded({ extended: true }));

  // опять отлавливаем ошибки которые может выкинуть модель "bodyParser.urlencoded"
    app.use (function (error, req, res, next){
        if( error ){
            console.error(` #main: [1]: [broken-url-data] ${error.message}`);
            return res.json({success: false, message: 'broken-url-data' });
        }
        next();
    });

    // тут можешь хоть миллион таких домулей прикрутить, как кастомных так и написанных другими людьми

    // вот простой пример того, как можно разрешить делать CORS запросы, и получить базовую 
    // информаци об клиенте который делает запрос ...
    app.use( async function(req, res, next){ 
        try{
        
            // CORS Headers
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', '*');
            res.header('access-control-allow-methods', 'GET,PUT,POST,HEAD,DELETE,OPTIONS');
            res.header('X-Powered-By', 'CamelHome');

            // собрал чуть-чуть данных об коннекте
            const method = (''+req.method).trim().toLowerCase();
            const path = (''+req.path);
            const origin = (req.headers['origin'] || '');
            const originDomain = origin ? origin.split('://')[1] : '';
            const protocol = origin ? origin.split('://')[0] : (req.headers['x-forwarded-proto'] || '');
            const secure = (protocol === 'https' );
            const host = req.headers['x-forwarded-server'] || req.headers['host'] || 'n/a';
            const ip = (req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress || 'n/a');

            // можно вывести все что угодно, или писать это в файловык логи ...
            console.log(` [${method}] ip: [${ip}] [${path}]`);

            // это обязательно => 
            return next();
        
            // если нужно остановить данные запрос, можно выкинуть вот так ошибку
            // return next(`Server error ...`);
        }catch(e){
            // но лучше выкдывать ошибку вот так, что бы она была в рамках всего API. 
            res.json({success: false, message:'server-error', data: {}});
        }
    });

    // передаем созданный app, назад в родителя
    return app;
}