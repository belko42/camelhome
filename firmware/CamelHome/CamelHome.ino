// LIBRARY
#include <ESP8266WiFi.h>     // WiFi инициализация
#include <ESP8266mDNS.h>     // mDNS протокол
#include <WiFiUdp.h>         // UDP протокол
#include <ArduinoOTA.h>      // Прошивка по воздуху OTA
#include "EspMQTTClient.h"   // MQTT Client
#include <Wire.h>            // i2c шина
#include "DHT.h"             // датчик влажности/температуры DHT22
//#include <Adafruit_BMP280.h> // датчик температуры/барометр  BMP280

// VARIABLE
#define SERIAL_BOD 115200    // Скорость Serial порта

/*
 * WIFI
 */
const char* ssid     = "BikeAuto(+79001001010)"; // SSID Wifi
const char* password = "9001001010";             // Password WiFi

/*
 * MQTT
 */
const char* mqtt_ip   = "217.25.89.129";          // MQTT IP
const char* mqtt_user = "camelhome";             // MQTT User
const char* mqtt_pass = "camelhome";             // MQTT Password
const int   mqtt_port = 1883;                    // MQTT Port

const char* device_name = "CamelHome_censor";         // Device Name

/*
 * DEVICE MQTT
 */
const String device_user = "camelhome"; // login user camelhome.ru
const String device_uid  = "62b6ab";    // device uid camelhome.ru

const String device = device_user + "/" + device_uid + "/";


/*
 * DHT22 - Temp, Humidity
 */
unsigned long millis_dht = 0;    // сколько прошло времени
const long interval_dht = 15000; // итерация раз в 15сек
#define DHTPIN D4                // DATA Pin DHT
#define DHTTYPE DHT22            // TYPE DHT
//#define DHTTYPE DHT21            // TYPE DHT

/*
 * BMP280 - Temp, Pressure, Altitude
 */
//#define BMP_SDA 12               // SDA Pin BMP
//#define BMP_SCK 14               // SCK Pin BMP
//unsigned long millis_bmp = 0;    // сколько прошло времени
//const long interval_bmp = 30000; // итерация раз в 30сек

/*
 * GERKON - Open/Close Door
 */
unsigned long millis_gerkon = 0;   // сколько прошло времени
const long interval_gerkon = 5000; // итерация раз в 5сек
#define GERKON_PIN 4               // DATA Pin GERKON

// FUNCTION
/*
 * START MQTT Client
 */
EspMQTTClient client(
  ssid,        // SSID WiFi
  password,    // PassWord Wifi
  mqtt_ip,     // MQTT server ip
  mqtt_user,   // MQTT UserName
  mqtt_pass,   // MQTT PassWord
  device_name, // Device Name
  mqtt_port    // MQTT Port
);

/*
 * START DHT22
 */
DHT dht(DHTPIN, DHTTYPE);

/*
 * START BMP280
 */
//Adafruit_BMP280 bmp;


void setup() {
  Serial.begin(SERIAL_BOD); // Start Serial
  
  /*
   * WIFI START
   */
  Serial.println("Загрузка...");
  WiFi.mode(WIFI_STA); //Wifi режим STA
  WiFi.begin(ssid, password); // Start Wifi

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Wifi - ошибка соединения!!! Перезагрузка...");
    delay(5000);
    ESP.restart();
  }

  /*
   * ArduinoOTA(прошивка по воздуху) START
   */
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }
    Serial.println("Start updating " + type);
  });
  
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  
  ArduinoOTA.begin(); // Start OTA protocol
  
  Serial.println("Я готов!!!");
  Serial.print("Мой IP адрес: ");
  Serial.println(WiFi.localIP());

  client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
  client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overrited with enableHTTPWebUpdater("user", "password").
  
  String lastwill = device + "lastwill";
  const char* _lastwill = lastwill.c_str();
  client.enableLastWillMessage(_lastwill, "1");  // You can activate the retain flag by setting the third parameter to true

  // Start
  dht.begin();                  // DHT22 Start

  //Wire.begin(I2C_SDA, I2C_SCK, 0x01); // I2C Start

  /*if (!bmp.begin(0x76)) {
    Serial.println(F("Не могу найти BMP280 сенсор. Проверяйте подключение."));
    while (1);
  }*/
  
  /* Default settings from datasheet. */
  //bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,      //Operating Mode.
                  //Adafruit_BMP280::SAMPLING_X16,     //Temp. oversampling
                  //Adafruit_BMP280::SAMPLING_X16,     //Pressure oversampling
                  //Adafruit_BMP280::FILTER_X16,       //Filtering.
                  //Adafruit_BMP280::STANDBY_MS_1000); //Standby time.
}

void onConnectionEstablished() {
  //client.publish("ROOT/BEDROOM/Light/status", "1"); // You can activate the retain flag by setting the third parameter to true
  client.subscribe(device + "lamp/status", [](const String & payload) {
    if (payload == "false") {
      pinMode(5, INPUT);
      digitalWrite(5, HIGH);
      Serial.println("Я выключил свет");
    } else if (payload == "true") {
      pinMode(5, OUTPUT);
      digitalWrite(5, LOW);
      Serial.println("Я включил свет");
    }
    Serial.println(payload);
  });

  /*client.subscribe("ROOT/BEDROOM/Buzzer", [](const String & payload1) {
    if (payload1 == "0") {
      
    }else if(payload1 == "1") {
      
    }
  });*/

  // Subscribe to "mytopic/wildcardtest/#" and display received message to Serial
  /*client.subscribe("mytopic/wildcardtest/#", [](const String & topic, const String & payload) {
    Serial.println(topic + ": " + payload);
    });*/

  // Publish a message to "mytopic/test"
  //client.publish("room/test1", "0"); // You can activate the retain flag by setting the third parameter to true

  // Execute delayed instructions
  /*client.executeDelayed(5 * 1000, []() {
    client.publish("mytopic/test", "This is a message sent 5 seconds later");
    });*/
}

void loop() {
  ArduinoOTA.handle(); // OTA - в лупе
  client.loop();       // Wifi MQTT - в лупе
  // I2C NANO читаем ВСЕГДА
  //Wire.requestFrom(9, 1);    // запрос 6 байт от слейва #9
  /*while(Wire.available()){    // пока есть, что читать
    char c = Wire.read();    // получаем байт (как символ)
    Serial.print(c);         // печатает в порт
  }*/
  // DHT22 выполняем раз в 15сек
  if (millis() - millis_dht >= interval_dht){
    millis_dht = millis();
    Serial.println("DHT22-Start");
    /*Тут код*/
    String h = String(dht.readHumidity());        // Влажность %
    String t = String(dht.readTemperature());     // Темп Цельсий
    String f = String(dht.readTemperature(true)); // Темп Фаренгейт
    //String h = "50";
    //String t = "25.5";

    if (h=="" || t=="" || f=="") {
      Serial.println(F("Failed to read from DHT sensor!"));
    }else{
      client.publish(device + "humidity/percent", h); // You can activate the retain flag by setting the third parameter to true
      client.publish(device + "temp/celsius", t); // You can activate the retain flag by setting the third parameter to true
      client.publish(device + "temp/fahrenheit", f); // You can activate the retain flag by setting the third parameter to true
    }
    /*\Тут код*/
  }
  // I2C BMP280 выполняем раз в 30сек
  /*if (millis() - millis_bmp >= interval_bmp){
    millis_bmp = millis();
    Serial.println("BMP280-Start");*/
    /*Тут код*/
    /*float tc = bmp.readTemperature(); // Темп Цельсий
    String tt = String(tc);           // Перевод в строку
    
    float pr = bmp.readPressure();    // Давление Паскаль
    String p = String(pr);            // Перевод в строку
    
    float mmHG = pr * 0.00750062;     // Давление мм.рт.ст
    String mm = String(mmHG);         // Перевод в строку

    if (tt=="" || p=="" || mm==""){
      Serial.println(F("Failed to read from BMP280 sensor!"));
    }else{
      client.publish("ROOT/CORRIDOR/Pressure/Pascal", p); // You can activate the retain flag by setting the third parameter to true
      client.publish("ROOT/CORRIDOR/Pressure/mmHG", mm); // You can activate the retain flag by setting the third parameter to true
      client.publish("ROOT/CORRIDOR/Temp/Celsius2", tt); // You can activate the retain flag by setting the third parameter to true
      //Serial.println("MQTT BMP280");
    }*/
    /*\Тут код*/
  //}
  // DIGITAL Gerkon выполняем раз в 1сек
  if (millis() - millis_gerkon >= interval_gerkon){
    millis_gerkon = millis();
    Serial.println("Gerkon-Start");
    int g = digitalRead(GERKON_PIN);
    String gerkon = String(g);
    client.publish(device + "door/status", gerkon);
  }
}
