const logger = require('mii-logger.js');
const fs = require('fs');
const path = require('path');

const config = require('./config');

const mqtt = require('mqtt');

const client = mqtt.connect(config.mqtt.host, config.mqtt.option);
client.on("connect", () => {console.log('connected MQTT');});

const { MongoClient, ObjectID } = require('mongodb');
//const ObjectId = require('mongodb').ObjectID;

const urlMongo = 'mongodb://'+ config.mongodb.username + ':' + config.mongodb.password + '@' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.dbName;
const mongoDB = new MongoClient(urlMongo, { useUnifiedTopology: true });

//client.subscribe('test/lamp');
client.subscribe('#');

const doStuff = async (topic, message, packet) => {
    try {
        console.log("Received '" + message + "' on '" + topic + "'");

        const split_topic = topic.split('/');
        const user = split_topic[0];
        const device = split_topic[1];
        const action = split_topic[2];
        const action_type = split_topic[3];
        const mess = parseFloat(message.toString());

        console.json({user, device, action, action_type, mess});

        const client = await mongoDB.connect();
        const db = client.db(config.mongodb.dbName);
        const coll_device = db.collection("device");
        const coll_status_device = db.collection("status_device");
        const result = await coll_device.findOne({"uid": device});
        //console.json(result);
        if(!result){ throw new Error('NULL result'); }

        switch(action) {
            case 'temp':
                if(action_type != 'celsius'){ throw new Error('no celsius'); }

                console.log("TEMP");
                const temp_res = await coll_status_device.updateMany(
                    {
                        "id_device": ObjectID(result._id),
                        "properties.state.instance": "temperature",
                    }, //поиск   
                    { $set: {"properties.$.state.value": mess} }, //что заменить
                    { upsert: false } //странная настройка
                );
                console.log("Готово");
                break;
            case 'door':
                console.log("DOOR");
                break;
            case 'humidity':
                if(action_type != 'percent'){ throw new Error('no percent'); }

                console.log("HUMIDITY");
                const hum_res = await coll_status_device.updateMany(
                    {
                        "id_device": ObjectID(result._id),
                        "properties.state.instance": "humidity",
                    }, //поиск   
                    { $set: {"properties.$.state.value": mess} }, //что заменить
                    { upsert: false } //странная настройка
                );
                console.log("Готово");
                break;
            case 'status':
                console.log("PING");
                break;
            default:
                console.log("ERR");
                throw new Error('err default');
        }
    } catch (e) {
        console.log(` error: [mqtt-daemon.js]: ${e.message} `);
    }
}

client.on('message', doStuff);
// Close the connection
mongoDB.close();