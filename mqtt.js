//const mqtt = require('mqtt');
const mqtt = require('async-mqtt');

module.exports = async ( App ) => {
    try{
        const host   = process.env.MQTT_HOST;
        const option = {
            port     : process.env.MQTT_PORT,
            username : process.env.MQTT_USERNAME,
            password : process.env.MQTT_PASSWORD,
            clientID : 'mqttjs_' + Math.random().toString(16).substr(2, 8)
        };

        const MQTT = await mqtt.connectAsync(host, option,);

        console.ok(' MQTT - init ');

        return MQTT;
    }catch(e){
        console.error(` MQTT: ${e.message} `);
        return false;
    }
}