const fs = require('fs');
const subdomain = require('express-subdomain');
const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');

const apiRoutes = require('./apiRoutes');

const { ObjectID } = require('mongodb');
const DB = require('./db');

const MQTT = require("./mqtt");

const express_config = require('./express-config');

module.exports = restApi = class restApi {
    constructor( config ) {
        //this.app.use(bodyParser.json());
        //this.app.use(bodyParser.urlencoded({ extended: true }));

        this.config = config;

        this.app = null;
        this.DB = null;
        this.MQTT = null;

        this.init();
    }

    async init() {

        this.DB = await DB( this );
        this.DB.ObjectID = (id) => { return ObjectID(id) };
        this.DB.ObjectId = (id) => { return ObjectID(id) };

        this.MQTT = await MQTT( this );

        // [init subdomains]
        this.app = express();

        this.app = await express_config( this );

        //this.app.use(bodyParser.json());
        //this.app.use(bodyParser.urlencoded({ extended: true }));

        this.app.api = express.Router();
        this.app.use(subdomain('api', this.app.api));

        //this.app.dev = express.Router();
        //this.app.use(subdomain('dev', this.app.dev));
        await apiRoutes( this );

        //Routes( this );

        // this.app.use(subdomain('api', Auth));
        // this.app.use(subdomain('api', Auth));
        // this.app.use('/smarthome', Auth);
        // this.app.use('/smarthome/api', Routes);

        this.app.listen(process.env.EXPRESS_PORT, () => {
                console.ok('#server https://www.camelhome.ru/ started on port', process.env.EXPRESS_PORT);
        });
    }
}