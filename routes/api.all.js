const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/
    router.all("/", async(req, res) => {
        try {
            console.ok('https://api.camelhome.ru/');
            console.line();
            //const collection = await App.DB.collection("auth");
            //const find = await collection.findOne({"login" : "camelhome"});
            //console.json(find);
            console.info("Response - OK")
            res.end("OK");
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(500)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    return router;
}