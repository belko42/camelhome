const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/auth
    router.get("/auth", async(req, res) => {
        try{
            console.ok('https://api.camelhome.ru/auth');
            console.line();

            console.json(req.query);

            let state = req.query.state;
            let redirect_uri = req.query.redirect_uri;

            const collection = App.DB.collection("auth");
            let LOGIN = 'camelhome';
            //let uid = '602edcbf6241ef3dd6dabee8';
            const result = await collection.findOne( { login: LOGIN } );
            console.log(result);
            if(result){
                console.info("Response - OK")
                res.send(
                    '<a style="display:block; text-align:center; background:#7b3dcc; color:#fff; cursor:pointer; font-family:Arial,sans-serif; font-size:20px; padding:13px 16px; border-radius:6px; text-decoration:none;" href="' + redirect_uri + '?code='+ result.code +'&state=' + state + '">Подключить умный дом</a>'
                );
            }else{
                console.error('ERROR AUTH NO LOGIN');
                res.json();
            }
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    return router;
}