const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/v1.0/user/devices
    router.get("/v1.0/user/devices", async(req, res) => {
        try {
            console.ok('https://api.camelhome.ru/v1.0/user/devices');
            console.line();

            console.log(JSON.stringify(req.headers));
            console.log(req.headers.authorization);
            console.log(req.headers['x-request-id']);
            if(!req.headers.authorization){
                console.error('ERROR devices NO auth token');
                return res.status(401).json({error: true});
            }

            let authorization = req.headers.authorization.split(" ");
            let token = authorization[1];

            const auth_collection = App.DB.collection("auth");
            const device_collection = App.DB.collection("device");

            const result = await auth_collection.findOne({token: token});

            console.log('результат ИД:', result._id);

            const all_dev_find = device_collection.find({id_user: result._id});

            const devices = [];
            await all_dev_find.forEach((doc) => {
                let ids = doc._id;
                delete doc._id;
                delete doc.id_user;
                devices.push({
                    id: ''+ ids,
                    ...doc,
                });
                //console.log('dump', doc)
            });
            //console.log('dump ALLDEV:', devices);
            const req_json = {
                "request_id": req.headers['x-request-id'],
                payload: {
                    user_id: result._id,
                    devices: devices,
                }
            }
            console.log('dump request:', req_json);

            console.info('Response - OK');

            res.json(req_json);
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    return router;
}