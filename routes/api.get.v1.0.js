const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/v1.0/
    router.get("/v1.0", async(req, res) => {
        try {
            console.ok('https://api.camelhome.ru/v1.0/');
            console.line();
            console.info("Response - OK");
            res.end("OK");
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    return router;
}