const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/token
    router.post("/token", async(req, res) => {
        try{
            console.ok('https://api.camelhome.ru/token');
            console.line();

            const data = req.body;
            console.json(data);

            const collection = App.DB.collection("auth");
            const result = await collection.findOne({code: data.code});
            console.log(result);
            if(!result){
                console.error('ERROR TOKEN NO CODE');
                return res.json({error: true, message: "ERROR TOKEN NO CODE"});
            }

            console.info('Response - OK');
            
            res.json({
                access_token: result.token,
                token_type: "bearer",
                expires_in: 2592000,
                refresh_token: result.token
            });

        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    return router;
}