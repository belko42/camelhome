const express = require('express');
const router = express.Router();

//const config = require('../config');
//const mqtt = require('mqtt');
//const mqtt_conn = mqtt.connect(config.mqtt.host, config.mqtt.option);
//mqtt_conn.on("connect", () => {console.ok('connected MQTT');});

module.exports = async ( App ) => {
    // https://api.camelhome.ru/v1.0/user/devices/action
    router.post("/v1.0/user/devices/action", async (req, res) => {
        try {
            console.ok('https://api.camelhome.ru/v1.0/user/devices/action');
            console.line();

            console.json(req.body);

            const devices = req.body.payload.devices[0];
            const id_device = devices.id;
            const type_action = devices.capabilities[0].type;
            const status_action = devices.capabilities[0].state.value;

            console.log(id_device);
            console.log(type_action);
            console.log(status_action);

            if(type_action != 'devices.capabilities.on_off'){
                throw new Error("no type capabilities");
            }

            const auth_collection = App.DB.collection("auth");
            const device_collection = App.DB.collection("device");
            const status_dev_collection = App.DB.collection("status_device");

            const status_dev_result = await status_dev_collection.updateMany(
                {"id_device": App.DB.ObjectID(id_device), "capabilities.type": type_action}, //поиск   
                { $set: {"capabilities.$.state.value": status_action} }, //что заменить
                { upsert: false }
            );

            const device_result = await device_collection.findOne({ _id: App.DB.ObjectID(id_device) });
            const auth_result = await auth_collection.findOne({ _id: App.DB.ObjectID(device_result.id_user) });
            
            //console.json(result);
            const a_login = auth_result.login;
            const d_uid   = device_result.uid;

            App.MQTT.publish(a_login+'/'+d_uid+'/lamp/status', ''+status_action, {qos: 2}, function() {
                console.log("Message is published");
                res.json(
                    {
                        "request_id" : req.headers['x-request-id'],
                        "payload" : {
                            "devices" : [
                                {
                                    "id" : id_device,
                                    "capabilities": [
                                        {
                                            "type": "devices.capabilities.on_off",
                                            "state" : {
                                                "instance": "on",
                                                "action_result": {
                                                    "status": "DONE",
                                                    //"status": "ERROR",
                                                    //"error_code": "DEVICE_UNREACHABLE",//устройство без сети
                                                    //"error_message": "Устройство, не коннект", //описание ошибки
                                                }
                                            }
                                        },
                                    ],
                                }
                            ]
                        }
                    }
                );
            });
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }

            /*client.subscribe('test/#', function() {
                // when a message arrives, do something with it
                client.on('message', function(topic, message, packet) {
                    console.log("Received '" + message + "' on '" + topic + "'");
                });
            });*/
            /********
            mqtt_conn.publish('test/lamp', 'NODE test mess', {qos: 2}, function() {
            console.log("Message is published");
            *******/
    });

    return router;
}