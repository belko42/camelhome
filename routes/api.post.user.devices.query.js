const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/v1.0/user/devices/query
    router.post("/v1.0/user/devices/query", async(req, res) => {
        try {
            console.ok('https://api.camelhome.ru/v1.0/user/devices/query');
            console.line();

            console.log(req.headers.authorization);
            console.log(req.headers['x-request-id']);

            if(!req.body){
                console.error('ERROR devices/query NO body');
                return res.status(401).json({error: true});
            }

            const device_collection = App.DB.collection("device");
            const status_dev_collection = App.DB.collection("status_device");

            console.log('DEVICES:', req.body.devices[0].id);
            const all_dev_find = await device_collection.find({'_id': App.DB.ObjectID(req.body.devices[0].id)}).toArray();

            const devices = [];
            //console.json({ all_dev_find });
            for( const doc of all_dev_find ){

                const stat_dev_find = await status_dev_collection.findOne({'id_device': doc._id});
                //console.log('STATDEV', stat_dev_find);
                let ids = doc._id;
                delete doc._id;
                delete doc.id_user;

                devices.push({
                    id: ''+ids,
                    name: doc.name,
                    description: doc.description,
                    room: doc.room,
                    type: doc.type,
                    capabilities: stat_dev_find.capabilities,
                    properties: stat_dev_find.properties,
                    device_info: doc.device_info
                });
                //console.json({ devices });

            }

            const req_json = {
                "request_id": req.headers['x-request-id'],
                payload: {
                    //user_id: result._id,
                    devices: devices,
                }
            }
            console.json(req_json);

            console.info('Response - OK');

            res.json(req_json);
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
        
        /*res.json(
            {
                "request_id" : req.headers['x-request-id'],
                "payload" : {
                    "user_id" : "1",
                    "devices" : [
                        {
                            "id" : "1",
                            "name" : "Свет",
                            "description" : "Свет в бане",
                            "room" : "Баня",
                            "type" : "devices.types.sensor",
                            /*"custom_data" : {},*/
                            /*"capabilities": [
                                {
                                    "type": "devices.capabilities.on_off",
                                    //"retrievable": true,
                                    "state" : {
                                        "instance": "on",
                                        "value": true
                                    }
                                },
                            ],
                            "properties": [
                                {
                                    "type": "devices.properties.float",
                                    "state": {
                                        "instance": "temperature",
                                        "value": 101.5
                                    }
                                },
                                {
                                    "type": "devices.properties.float",
                                    "state": {
                                        "instance": "humidity",
                                        "value": 85
                                    }
                                }
                            ],
                            "device_info": {
                                "hw_version": "0.1",
                                "sw_version": "0.1"
                            }
                        }
                    ]
                }
            }
        );*/
    });

    return router;
}