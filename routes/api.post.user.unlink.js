const express = require('express');
const router = express.Router();

module.exports = async ( App ) => {
    // https://api.camelhome.ru/v1.0/user/unlink
    router.post("/v1.0/user/unlink", async(req, res) => {
        try {
            console.ok('https://api.camelhome.ru/v1.0/user/unlink');
            console.line();
            
            console.info("Response - OK");
            res.json({
                request_id : req.headers['x-request-id']
            });
        }catch(e) {
            console.error(` error: [${req.path}]: ${e.message} `);
            return res
            .status(502)
            .json({success: false, message: 'Server error', data: {}});
        }finally {
            console.line();
            console.logTime( false );
            console.log(); console.log();
            console.logTime( true );
        }
    });

    return router;
}